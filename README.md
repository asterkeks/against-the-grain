# Iota Signus

WS:  41
BS:  36
S:   43
T:   41
Ag:  32
Int: 51
Per: 37
Wil: 35
Fel: 37

Wounds: 12/12
Fate: 3/3

# Demeanour:

Fixed

## Call:

Xenos tech to be disassembled for the underlying principals stolen from the Omnissiah.

## Why on a Rogue Trader

He is a resourceful man, and with his theory skirting the edge of tech heresy not the most liked man among the admech
so 'loaning' him to a rt seemed natural at the time. And he has had history with Archibald

## Sacrifice

He has come to see his slow removal from humanity through augmentation to be the sacrifice he must make in order to
perform his vital role on the ship and in the universe

## Ambition

Shipbuilding principles to improve the ships to the scale of Ark Mechanicus

## Hatred

Use of unaltered Xenotech, especialy by those deemed unworthy by the logical Eyes of the Omnissiah - mainly Noblemen and
other diplomatic scum

# Skills

+ Common Lore(Machine Cult, Tech) (Int)
+ Common Lore (Tech) - Basic
+ Common Lore (Machine Cult) - Basic
    - -10 to Knowledge Imperial Creed
    - -5 Interaction Ecclesiary(formal)
+ Intimidate(trained)
+ Forbidden Lore (Archeotech, Adeptus Mechanicus)
+ Literacy (Int)
+ Logic(Int)
+ Speak Language(Explorator Binary, Low Gothic, Techna-lingua) (Int)
+ Tech-Use (Int)
+ Trade (Technomat) (Int)
+ Tracking(Trained)
+ Forbidden Lore(the Warp)
+ Scholastic Lore(Astromancy)
+ Trade Armourer 100ep
+ Awareness(trained)

# Talents

+ Quick Draw
+ Sprint
+ Basic Weapon training(Universal)
+ Melee Weapon Training (Universal)

# Traits

+ Peer(Admech)
+ Mechanicus Implants (p366)
+ Unnatural Intelligence(x2)
+ Calculus Logi Upgrade (from: best quality Cortex Implants)

# Equipment

+ Good Quality Utility Mechadentrite - initial requisition
+ Best Quality Cortex Implants - explorator special skill (400ep for best quliaty)
+ Best Quality Memorance Implant
+ Boltgun
+ Good Power Axe
+ Enforcer light carapace
+ Multikey
+ Voidsuit
+ Injector
+ Sacret Unguents
+ Micro-bead
+ Combi-Tool
+ Dataslate
+ "Variable" Servo Skull Familiar with Auspex

# Fate Log

+ Page 12 Dodge demonic gun 1/3 -> 0/3

# Insanity Log

+ 1d5 insanity - Stubjack
  + 2
+ 1d5 insanity - Dark Voyage
  + 5
+ 1d10 insanity - Best quality cortex implants
  + 2

# Links

- [Story](./Story.md)
- [Creation](./Creation.txt)